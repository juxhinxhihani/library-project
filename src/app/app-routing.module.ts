import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from "./components/home/home.component";
import { BookListComponent } from "./books/book-list/book-list.component";
import { CreateBookComponent } from "./books/create-book/create-book.component";
import { LoginComponent } from "./login/login/login.component";
import {BookComponent} from "./books/book/book.component";
import {PageNotFoundComponent} from "./components/page-not-found/page-not-found.component";
import {AuthGuardService} from "./services/auth-guard.service";

const routes: Routes = [
  { path: 'home', component: HomeComponent, canActivate: [AuthGuardService] },
  { path: 'books', component: BookListComponent, canActivate: [AuthGuardService] },
  { path: 'createBook', component: CreateBookComponent, canActivate: [AuthGuardService] },
  { path: 'login', component: LoginComponent },
  { path: 'book/:isbn', component: BookComponent, canActivate: [AuthGuardService] },
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
