import {Component, OnInit} from '@angular/core';
import {AuthenticateService} from "../../services/authenticate.service";
import {Router} from "@angular/router";
import {UserServiceService} from "../../services/user-service.service";
import {UserModel} from "../../_models/user.model";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  allUsers: UserModel[] = [];

  constructor(private authService: AuthenticateService,
              private router: Router,
              private userService: UserServiceService) {
  }

  ngOnInit(): void {
    this.userService.getAllUsers().subscribe((response: UserModel[]) => {
      this.allUsers = response;
      console.log(response)
    })

    this.userService.getMockUsers().subscribe( response => {
      console.log(response)
    })
  }

  login(email: string, password: string) {
    this.allUsers.forEach((user: UserModel) => {
      if (user.email === email && user.password === password) {
        this.authService.setLoggedIn(true);
        this.router.navigate(['/home']);
      } else {
        console.error("User does not exist")
      }
    })
  }
}
