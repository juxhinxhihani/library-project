export interface UserModel {
  email: string;
  password: string;
  name: string;
  lastName: string;
  birthDate?: Date;
  gender?: string;
  username: string;
}
