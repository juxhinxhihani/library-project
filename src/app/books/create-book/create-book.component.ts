  import { Component, OnInit } from '@angular/core';
  import {FormArray, FormBuilder, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-create-book',
  templateUrl: './create-book.component.html',
  styleUrls: ['./create-book.component.css']
})
export class CreateBookComponent implements OnInit {
  // Listen for changes on each control when rxjs and observables will be explained
  // Custom validators were given as Home Work

  public createBookForm: FormGroup = this.fb.group({
    books: this.fb.array([])
  });
  public genres: string[] = [
    'Non Fictional',
    'Thriller',
    'Classics',
    'Fantasy',
    'Adventure'
  ];
  public submitted: boolean = false;

  constructor(
    private fb: FormBuilder
  ) {
  }

  ngOnInit(): void {
    this.initBookForm();
  }

  private initBookForm() {
    this.addNewBook();
  }

  public addNewBook() {
    this.booksFormArray.push(this.newBook());
  }

  private newBook() {
    return this.fb.group({
      isbn: ['', Validators.required],
      author: ['', Validators.required],
      title: ['', Validators.required],
      year: ['', Validators.required],
      genre: ['']
    })
  }

  public get booksFormArray(): FormArray {
    return this.createBookForm.get('books') as FormArray;
  }

  public onSubmit(): void {
    this.submitted = true;
    if (this.createBookForm.valid) {
      console.log(this.createBookForm.value);
    } else {
      console.log('Form is not valid');
    }
  }

  public onReset(): void {
    //Any subscriptions on your formArray,
    // such as that registered with formArray.valueChanges, will not be lost.
    while (this.booksFormArray.length !== 0) {
      this.booksFormArray.removeAt(0)
    }

    this.submitted = false;

    this.initBookForm();
  }

  public removeBook(i: number): void {
    if (this.booksFormArray.length > 1) {
      this.booksFormArray.removeAt(i);
    } else {
      this.onReset();
    }
  }

}
