import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ReactiveFormsModule} from "@angular/forms";
import { CreateBookComponent } from './create-book/create-book.component';
import { BookListComponent } from './book-list/book-list.component';
import { BookComponent } from './book/book.component';
import {AvailabilityPipe} from "../_pipes/availability.pipe";



@NgModule({
  declarations: [
    CreateBookComponent,
    BookListComponent,
    BookComponent,
    AvailabilityPipe
  ],
  exports: [
    CreateBookComponent,
    BookListComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule
  ]
})
export class BooksModule { }
