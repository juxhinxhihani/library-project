import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Book} from "../../_models/book.model";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.css']
})
export class BookComponent implements OnInit {
  @Input() book: Book | undefined;
  @Output() changeAvailability: EventEmitter<string> = new EventEmitter<string>();

  public books: Book[] = [
    {
      isbn: '0553213695',
      author: 'Franz Kafka',
      title: 'The Metamorphosis ',
      year: 'Published March 1st 1972 by Bantam Classics',
      genre: 'Classics',
      available: true
    },
    {
      isbn: '0811201880',
      author: 'Jean-Paul Sartre',
      title: 'Nausea',
      year: 'Published 1969 by New Directions',
      genre: 'Philosophy',
      available: false
    }
  ]
  edit = false;
  constructor(private route: ActivatedRoute) { }

  ngOnInit(): void {
    const isbn: string = this.route.snapshot.params['isbn'];
    this.book = this.books.filter(el => el.isbn === isbn)[0];
    this.edit = this.route.snapshot.queryParams['editing'] === 'true';
  }

  onChangeAvailability() {
    this.changeAvailability.emit(this.book?.isbn);
  }

}
