import {Injectable} from "@angular/core";

@Injectable({providedIn: 'root'})
export class AuthenticateService {
  loggedIn = false;

  isLoggedIn():boolean {
    return  this.loggedIn;
  }

  setLoggedIn(loggedIn: boolean): void {
    this.loggedIn = loggedIn;
  }
}
